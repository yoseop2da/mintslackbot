//
//  SlackViewController.m
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import "SlackViewController.h"
#import "InformationTableViewController.h"
#import "TransPort.h"

@interface SlackViewController ()

@property (strong) NSDictionary *slackData;

@end

@implementation SlackViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Hello Mint";
    
    UIImage *buttonImage = [UIImage imageNamed:@"leftButtonImage"];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setImage:buttonImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0,0.0,buttonImage.size.width,buttonImage.size.height);
    [aButton addTarget:self action:@selector(settings) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    self.navigationItem.leftBarButtonItem = backButton;
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftButtonImage"] landscapeImagePhone:[UIImage imageNamed:@"leftButtonImage"] style:UIBarButtonItemStylePlain target:self action:@selector(none)];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(settings)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self checkSlackData];
}

- (void)checkSlackData
{
    self.slackData = [[NSUserDefaults standardUserDefaults] objectForKey:kSlackData];
    
    if (self.slackData) {
        self.title = @"#Random";
    }else{
        [self settings];
    }
}

- (void)settings
{
    InformationTableViewController *informationViewController = [InformationTableViewController new];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:informationViewController];
    [self presentViewController:navController animated:YES completion:^{}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)sayGoodMorning:(id)sender {
    if (self.slackData) {
        [TransPort sayhelloWithDictionary:self.slackData isMorning:YES Completion:^(NSDictionary *result) {
            NSLog(@"result========%@",result);
        }];
    }
}

- (IBAction)sayGoodBye:(id)sender {
    if (self.slackData) {
        [TransPort sayhelloWithDictionary:self.slackData isMorning:NO Completion:^(NSDictionary *result) {
            NSLog(@"result========%@",result);
        }];
    }
}

- (void)none
{
    
}
@end
