//
//  InformationTableViewController.m
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 28..
//
//

#import "InformationTableViewController.h"
#import "InformationViewCell.h"
#import "InformationLabelViewCell.h"
#import "DetailTableViewController.h"
#import "SlackUser.h"

@interface InformationTableViewController ()

@property (strong) NSArray *firstSectionTitleFields;
@property (strong) NSArray *secondSectionTitleFields;
@property (strong) NSDictionary *slackData;

@property (strong) SlackUser *slackUser;

@end

@implementation InformationTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNib];
    [self setUpSlackData];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
    
    self.title = @"설정";
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.firstSectionTitleFields = @[@"Token",@"출근 Message",@"퇴근 Message"];
    self.secondSectionTitleFields = @[@"Channel",@"UserName"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)loadNib
{
    UINib *textFieldManageNib = [UINib nibWithNibName:NSStringFromClass([InformationViewCell class]) bundle:nil];
    [self.tableView registerNib:textFieldManageNib forCellReuseIdentifier:kInformationViewCellIdentifier];
    UINib *labelManageNib = [UINib nibWithNibName:NSStringFromClass([InformationLabelViewCell class]) bundle:nil];
    [self.tableView registerNib:labelManageNib forCellReuseIdentifier:kInformationLabelViewCellIdentifier];
}

- (void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)save
{
    NSString *token = self.slackUser.token;
    NSString *firstMessage = self.slackUser.firstMessage;
    NSString *secondMessage = self.slackUser.secondMessage;
//    NSString *channel = self.slackUser.channel[@"name"];
    NSString *userName = self.slackUser.userName[@"name"];

    if((token.length > 0)
       && (firstMessage.length > 0)
       && (secondMessage.length > 0)
//       && (channel.length > 0)
       && (userName.length > 0))
    {
        [self SaveSlackData];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"빈값이 있습니다."
                                    message:nil
                                   delegate:self
                          cancelButtonTitle:@"확인"
                          otherButtonTitles:nil, nil]show];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setUpSlackData
{
    self.slackData = [[NSUserDefaults standardUserDefaults] valueForKey:kSlackData];
    self.slackUser = [SlackUser new];
    
    if (self.slackData) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
        
        self.slackUser.token = self.slackData[@"token"];
        self.slackUser.firstMessage = self.slackData[@"firstMessage"];
        self.slackUser.secondMessage = self.slackData[@"secondMessage"];
        self.slackUser.channel = self.slackData[@"channel"];
        self.slackUser.userName = self.slackData[@"userName"];
        
    }else{
        
        self.slackUser.token = kMintechToken;
        self.slackData = [NSDictionary new];
    }
}

- (void)SaveSlackData
{
    self.slackData = @{
                       @"token": self.slackUser.token,
                       @"firstMessage": self.slackUser.firstMessage,
                       @"secondMessage": self.slackUser.secondMessage,
                       @"channel": @"G029Y3QLV",
//                       @"channel": self.slackUser.channel,
                       @"userName": self.slackUser.userName
                       };
    
    [[NSUserDefaults standardUserDefaults] setObject:self.slackData forKey:kSlackData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.firstSectionTitleFields.count;
    }else{
        return self.secondSectionTitleFields.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        InformationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kInformationViewCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleField.text = self.firstSectionTitleFields[indexPath.row];
        cell.textField.placeholder = self.firstSectionTitleFields[indexPath.row];
        switch (indexPath.row) {
            case 0:
            {
                cell.textField.text = self.slackUser.token;
            }
                break;
            case 1:
            {
                cell.textField.text = self.slackUser.firstMessage;
            }
                break;
            case 2:
            {
                cell.textField.text = self.slackUser.secondMessage;
            }
                break;
        }
        cell.textField.tag = indexPath.row;
        [cell.textField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
        return cell;
    }else{
        InformationLabelViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kInformationLabelViewCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleField.text = self.secondSectionTitleFields[indexPath.row];
        cell.textField.text = self.secondSectionTitleFields[indexPath.row];
        
        switch (indexPath.row) {
            case 0:
            {
                cell.textField.text = @"Random";
            }
                break;
            case 1:
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.textField.text = self.slackUser.userName[@"name"];
            }
                break;
        }
        return cell;
    }
    
}

- (void)textFieldChanged:(id)sender
{
    UITextField *textfield = sender;
    
    switch (textfield.tag) {
        case 0:
        {
            self.slackUser.token = textfield.text;
        }
            break;
        case 1:
        {
            self.slackUser.firstMessage = textfield.text;
        }
            break;
        case 2:
        {
             self.slackUser.secondMessage = textfield.text;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        
        DetailTableViewController *detailTableViewController = [DetailTableViewController new];
        detailTableViewController.slackUser = self.slackUser;
        
        switch (indexPath.row) {
            case 0:
            {
//                detailTableViewController.fieldName = kChannelField;
            }
                break;
            case 1:
            {
                detailTableViewController.fieldName = kUserNameField;
                [self.navigationController pushViewController:detailTableViewController animated:YES];
            }
                break;
                
            default:
                break;
        }
    }
}

@end
