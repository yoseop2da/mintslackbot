//
//  DetailTableViewController.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import <UIKit/UIKit.h>
#import "SlackUser.h"

@interface DetailTableViewController : UITableViewController

@property (strong) NSString *fieldName;
@property (strong) SlackUser *slackUser;

@end
