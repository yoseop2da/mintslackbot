//
//  InformationLabelViewCell.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import <UIKit/UIKit.h>

static NSString *kInformationLabelViewCellIdentifier = @"InformationLabelViewCellIdentifier";

@interface InformationLabelViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleField;
@property (weak, nonatomic) IBOutlet UILabel *textField;

@end
