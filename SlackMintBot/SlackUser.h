//
//  SlackUser.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import <Foundation/Foundation.h>

@interface SlackUser : NSObject

@property (strong) NSString *token;
@property (strong) NSString *firstMessage;
@property (strong) NSString *secondMessage;
@property (strong) NSDictionary *channel;
@property (strong) NSDictionary *userName;

@end
