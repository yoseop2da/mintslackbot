//
//  DetailTableViewController.m
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import "DetailTableViewController.h"
#import "TransPort.h"

#define kFirstItem 0

@interface DetailTableViewController ()
{
    int _selectedIndex;
}

@property (strong) NSArray *dataArray;
@property (strong) NSString *changedValue;
@end

@implementation DetailTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.fieldName;
    
    if ([self.fieldName isEqualToString:kChannelField]) {
        [self getChannel];
    }else{
        [self getUserName];
    }
    
    self.changedValue = self.dataArray[kFirstItem];
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"saved value is [ %@ ] ", self.dataArray[_selectedIndex]);
    if ([self.fieldName isEqualToString:kChannelField]) {
        self.slackUser.channel = self.dataArray[_selectedIndex];
    }else{
        self.slackUser.userName = self.dataArray[_selectedIndex];
    }
}

- (void)getChannel
{
    [TransPort allChannelWithCompletion:^(NSArray *allChannel) {
        NSLog(@"allChannel = %@",allChannel);
        self.dataArray = allChannel;
        [self.tableView reloadData];
    }];
}

- (void)getUserName
{
    [TransPort allUserWithCompletion:^(NSArray *allUser) {
        NSLog(@"allUser = %@",allUser);
        self.dataArray = allUser;
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row == _selectedIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    id dataAttributes = self.dataArray[indexPath.row];
    cell.textLabel.text = dataAttributes[@"name"];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.changedValue = self.dataArray[indexPath.row];
    
    _selectedIndex = indexPath.row;
    [tableView reloadData];

}

@end
