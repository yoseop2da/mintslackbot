//
//  InformationViewCell.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 28..
//
//

#import <UIKit/UIKit.h>

static NSString *kInformationViewCellIdentifier = @"InformationViewCellIdentifier";

@interface InformationViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleField;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end
