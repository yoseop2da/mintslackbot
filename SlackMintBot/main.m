//
//  main.m
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 28..
//
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
