//
//  TransPort.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import <Foundation/Foundation.h>

@interface TransPort : NSObject

+ (void)allUserWithCompletion:(void (^)(NSArray *allUser))completion;
+ (void)allChannelWithCompletion:(void (^)(NSArray *allChannel))completion;

+ (void)sayhelloWithDictionary:(NSDictionary *)slackData isMorning:(BOOL)morning Completion:(void (^)(NSDictionary *result))completion;
@end
