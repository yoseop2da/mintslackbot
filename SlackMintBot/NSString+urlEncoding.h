//
//  NSString+urlEncoding.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 30..
//
//

#import <Foundation/Foundation.h>

@interface NSString (urlEncoding)

- (NSString *)urlencode;

@end
