//
//  SlackViewController.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import <UIKit/UIKit.h>

@interface SlackViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *channelField;
@property (weak, nonatomic) IBOutlet UIButton *toMintButton;
@property (weak, nonatomic) IBOutlet UIButton *fromMintButton;

- (IBAction)sayGoodMorning:(id)sender;
- (IBAction)sayGoodBye:(id)sender;

@end
