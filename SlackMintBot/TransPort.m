//
//  TransPort.m
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 29..
//
//

#import "TransPort.h"
#import "NSString+urlEncoding.h"

@implementation TransPort

+ (void)allUserWithCompletion:(void (^)(NSArray *allUser))completion
{
    
    NSString *urlString = @"https://slack.com/api/users.list?token=xoxp-2335187162-2335211656-2354563697-41cb2d";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSError *error;

        id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        NSMutableArray *allUserArray = [NSMutableArray array];
        for (id members in jsonData[@"members"]) {
            NSDictionary *user = @{
                                   @"userId": members[@"id"],
                                   @"name": members[@"name"],
                                   @"realName": members[@"real_name"]
                                   };
            [allUserArray addObject:user];
        }
        completion(allUserArray);
        
    }];
}

+ (void)allChannelWithCompletion:(void (^)(NSArray *allChannel))completion
{
    
    NSString *urlString = @"https://slack.com/api/channels.list?token=xoxp-2335187162-2335211656-2354563697-41cb2d";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSError *error;
        
        id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        NSMutableArray *allChannelArray = [NSMutableArray array];
        for (id members in jsonData[@"channels"]) {
            NSDictionary *user = @{
                                   @"channelId": members[@"id"],
                                   @"name": members[@"name"]
                                   };
            [allChannelArray addObject:user];
        }
        completion(allChannelArray);
        
    }];
}

+ (void)sayhelloWithDictionary:(NSDictionary *)slackData isMorning:(BOOL)morning Completion:(void (^)(NSDictionary *result))completion
{
    NSString *message;
    if (morning) {
        message = [NSString stringWithFormat:@"<@%@> %@",slackData[@"userName"][@"userId"],slackData[@"firstMessage"]];
    }else{
        message = [NSString stringWithFormat:@"<@%@> %@",slackData[@"userName"][@"userId"],slackData[@"secondMessage"]];
    }

    message = [message urlencode];
    
    NSString *baseURL = @"https://slack.com/api/chat.postMessage?token=xoxp-2335187162-2335211656-2354563697-41cb2d";

    baseURL = [baseURL stringByAppendingFormat:@"&channel=%@",slackData[@"channel"]];
    baseURL = [baseURL stringByAppendingFormat:@"&text=%@", message];
    baseURL = [baseURL stringByAppendingFormat:@"&username=%@",slackData[@"userName"][@"name"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:baseURL]];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSError *error;
        
        id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        completion(jsonData);
        
    }];
}

@end
