//
//  AppDelegate.h
//  SlackMintBot
//
//  Created by ParkYoseop on 2014. 5. 28..
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
